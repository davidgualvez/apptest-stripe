@extends('layouts.app')

@section('title', 'Shop')

@section('js')
<script>
    $('.add-to-cart-product').on('click', function(){
        var self = $(this); 

        post("{{ route('add-to-cart') }}", {
            product_id: self.data('id'),
            qty:1 
        }, function(res){ 
            Swal.fire({
                position: 'bottom-end', 
                text: res.message,
                showConfirmButton: false,
                timer: 1500
            });
            getCartCount();
        });
    });
</script>
@endsection

@section('content')

    <!-- HEADER / SITEMAP -->
    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>Shop</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="javascript:;">Shop</a></li>
                        <li class="active">Items</li>
                    </ul>


                </div>
            </div>
        </div>
    </section>

    <div class="container">

        @include('layouts.alerts.alert')

    
        <div class="masonry-loader masonry-loader-loaded">
                <div class="row products product-thumb-info-list" data-plugin-masonry="" data-plugin-options="{'layoutMode': 'fitRows'}" style="position: relative; height: 1747.2px;">
                    {{--<div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 0px; top: 0px;">
                        <a href="shop-product-sidebar-left.html">
                            <span class="onsale">Sale!</span>
                        </a>
                        <span class="product-thumb-info border-0">
                            <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="shop-product-sidebar-left.html">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="img/products/product-grey-1.jpg">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="shop-product-sidebar-left.html">
                                    <h4 class="text-4 text-primary">Photo Camera</h4>
                                    <span class="price">
                                        <del><span class="amount">$325</span></del>
                                        <ins><span class="amount text-dark font-weight-semibold">$299</span></ins>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 360px; top: 0px;">
                        <span class="product-thumb-info border-0">
                            <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="shop-product-sidebar-left.html">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="img/products/product-grey-2.jpg">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="shop-product-sidebar-left.html">
                                    <h4 class="text-4 text-primary">Golf Bag</h4>
                                    <span class="price">
                                        <span class="amount text-dark font-weight-semibold">$72</span>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 0px; top: 437.2px;">
                        <span class="product-thumb-info border-0">
                            <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="shop-product-sidebar-left.html">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="img/products/product-grey-3.jpg">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="shop-product-sidebar-left.html">
                                    <h4 class="text-4 text-primary">Workout</h4>
                                    <span class="price">
                                        <span class="amount text-dark font-weight-semibold">$60</span>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 360px; top: 437.2px;">
                        <span class="product-thumb-info border-0">
                            <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="shop-product-sidebar-left.html">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="img/products/product-grey-4.jpg">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="shop-product-sidebar-left.html">
                                    <h4 class="text-4 text-primary">Luxury bag</h4>
                                    <span class="price">
                                        <span class="amount text-dark font-weight-semibold">$199</span>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 0px; top: 873.6px;">
                        <span class="product-thumb-info border-0">
                            <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="shop-product-sidebar-left.html">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="img/products/product-grey-5.jpg">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="shop-product-sidebar-left.html">
                                    <h4 class="text-4 text-primary">Ladies' handbag</h4>
                                    <span class="price">
                                        <span class="amount text-dark font-weight-semibold">$189</span>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 360px; top: 873.6px;">
                        <a href="shop-product-sidebar-left.html">
                            <span class="onsale">Sale!</span>
                        </a>
                        <span class="product-thumb-info border-0">
                            <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="shop-product-sidebar-left.html">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="img/products/product-grey-6.jpg">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="shop-product-sidebar-left.html">
                                    <h4 class="text-4 text-primary">Baseball Cap</h4>
                                    <span class="price">
                                        <del><span class="amount">$25</span></del>
                                        <ins><span class="amount text-dark font-weight-semibold">$22</span></ins>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 0px; top: 1310.8px;">
                        <span class="product-thumb-info border-0">
                            <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="shop-product-sidebar-left.html">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="img/products/product-grey-7.jpg">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="shop-product-sidebar-left.html">
                                    <h4 class="text-4 text-primary">Blue Ladies Handbag</h4>
                                    <span class="price">
                                        <span class="amount text-dark font-weight-semibold">$290</span>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 360px; top: 1310.8px;">
                        <span class="product-thumb-info border-0">
                            <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="shop-product-sidebar-left.html">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="img/products/product-grey-8.jpg">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="shop-product-sidebar-left.html">
                                    <h4 class="text-4 text-primary">Military Rucksack</h4>
                                    <span class="price">
                                        <span class="amount text-dark font-weight-semibold">$49</span>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>--}}

                    @foreach( $products as $key=>$p)
                    <div class="col-12 col-sm-6 col-lg-3 product" style="position: absolute; left: 360px; top: 1310.8px;">
                        <span class="product-thumb-info border-0">
                            <a 
                                data-id="{{ $p->id }}"
                                href="javascript:;" 
                                class="add-to-cart-product bg-color-primary">
                                <span class="text-uppercase text-1">Add to Cart</span>
                            </a>
                            <a href="javascript:;">
                                <span class="product-thumb-info-image">
                                    <img alt="" class="img-fluid" src="{{ $p->photo }}">
                                </span>
                            </a>
                            <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                <a href="javascript:;">
                                    <h4 class="text-4 text-primary">{{ $p->name }}</h4>
                                    <span class="price">
                                        <span class="amount text-dark font-weight-semibold">${{ $p->price }}</span>
                                    </span>
                                </a>
                            </span>
                        </span>
                    </div>
                    @endforeach

                </div>
                
                {{--
                <div class="row">
                    <div class="col">
                        <ul class="pagination float-right">
                            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>
                        </ul>
                    </div>
                </div>
                --}}

            <div class="bounce-loader">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

    </div>

@endsection