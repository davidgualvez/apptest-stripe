<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body border-color-primary border-bottom-0">
        {{--<div class="header-top header-top-simple-border-bottom">
            <div class="container">
                <div class="header-row py-2">
                <!-- <div class="header-column justify-content-start">
                    <div class="header-row">
                        <nav class="header-nav-top">
                            <ul class="nav nav-pills text-uppercase text-2">
                            <li class="nav-item nav-item-anim-icon d-none d-md-block">
                                <a class="nav-link pl-0" href="about-us.html"><i class="fas fa-angle-right"></i> About Us</a>
                            </li>
                            <li class="nav-item nav-item-anim-icon d-none d-md-block">
                                <a class="nav-link" href="contact-us.html"><i class="fas fa-angle-right"></i> Contact Us</a>
                            </li>
                            </ul>
                        </nav>
                    </div>
                </div> -->
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <nav class="header-nav-top">
                            <ul class="nav nav-pills">

                                <li class="nav-item">
                                    <a href="/submit-entry"><i class="fa fa-check text-4 text-color-primary" style="top: 0;"></i> Submit Entry</a>
                                </li> 

                            @if (Route::has('login'))   
                                @auth 
                                    <li class="nav-item">
                                        <a href="/user"><i class="fa fa-home text-4 text-color-primary" style="top: 0;"></i> Profile</a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="/logout"><i class="fa fa-sign--alt text-4 text-color-primary" style="top: 0;"></i> Logout</a>
                                    </li> 
                                @else 
                                    <li class="nav-item">
                                        <a href="/login"><i class="fa fa-sign-in-alt text-4 text-color-primary" style="top: 0;"></i> Login</a>
                                    </li>

                                    @if (Route::has('register')) 
                                        <li class="nav-item">
                                            <a href="/register"><i class="fa fa-user text-4 text-color-primary" style="top: 0;"></i> Register</a>
                                        </li>
                                    @endif
                                @endauth 
                            @endif  
                                

                            </ul>
                        </nav>
                    </div>
                </div>
                </div>
            </div>
        </div>
        --}}
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                <div class="header-row">
                    <div class="header-logo">
                        {{--
                        <a href="/">
                        <img alt="Porto" height="48" data-sticky-height="40" src="/img/logo-pcbattlezone-trans.png">
                        </a>
                        --}}
                    </div>
                </div>
                </div>
                <div class="header-column justify-content-end">
                <div class="header-row">
                    <div class="header-nav header-nav-line header-nav-bottom-line">
                        <div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                            <nav class="collapse">
                            <ul class="nav nav-pills" id="mainNav">
                                <li>
                                    <a class="dropdown-item dropdown-toggle" href="{{ route('shop') }}">
                                        Shop
                                    </a>
                                </li>

                                <li>
                                    <a class="dropdown-item dropdown-toggle" href="{{ route('cart') }}">
                                        Cart
                                        <span class="ml-1 badge badge-primary badge-sm" id="cart-count"></span>
                                    </a> 
                                </li>

                                <!-- <li>
                                    <a class="dropdown-item dropdown-toggle" href="{{ route('config') }}">
                                        Config
                                    </a>
                                </li> -->
                                
                                <!-- <li>
                                    <a class="dropdown-item dropdown-toggle" href="/about">
                                    About Us
                                    </a>
                                </li>

                                <li>
                                    <a class="dropdown-item dropdown-toggle" href="/winners">
                                    Winners
                                    </a>
                                </li>

                                <li>
                                    <a class="dropdown-item dropdown-toggle" href="/entries">
                                    Entries
                                    </a>
                                </li>

                                <li>
                                    <a class="dropdown-item dropdown-toggle" href="/contact-us">
                                    Contact Us
                                    </a>
                                </li>        -->
                            </nav>
                        </div>
                        <ul class="header-social-icons social-icons d-none d-sm-block">
                            <!-- <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                            <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li> -->
                        </ul>
                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                        <i class="fas fa-bars"></i>
                        </button>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</header>