<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title') | {{ config('app.name') }} </title>

    <!-- SEO -->
    <meta name="keywords"       content="keywords" />
    <meta name="description"    content="description">
    <meta name="author"         content="joserizal">
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/img/apple-touch-icon.png">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/vendor/animate/animate.min.css">
    <link rel="stylesheet" href="/vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="/vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.min.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="/css/theme.css">
    <link rel="stylesheet" href="/css/theme-elements.css">
    <link rel="stylesheet" href="/css/theme-blog.css">
    <link rel="stylesheet" href="/css/theme-shop.css">
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="/vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="/vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="/vendor/rs-plugin/css/navigation.css">
    <!-- Skin CSS -->
    <link rel="stylesheet" href="/css/skins/default.css">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="/css/custom.css">
    <!-- Head Libs -->
    <script src="/vendor/modernizr/modernizr.min.js"></script>

    <!-- CUSTOM CSS -->
    @yield('css')

    <style>
        .boxx{
            border: 1px solid gray;
        }
    </style>

</head>
<body class="body">

    <!-- <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=2262439563807524&autoLogAppEvents=1"></script> -->
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId={{ config('setup.fb_id') }}&autoLogAppEvents=1"></script>
    <!-- HEADER-NAVIGATION  -->
    @include('layouts.header-nav')

    <div role="main" class="main shop py-4"> 

        <!-- MAIN CONTENT -->
        @yield('content')

        <!-- FOOTER  -->
        {{--@include('layouts.footer')--}}

    </div>

    
    <!-- Vendor -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery.appear/jquery.appear.min.js"></script>
    <script src="/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="/vendor/jquery.cookie/jquery.cookie.min.js"></script>
    <script src="/vendor/popper/umd/popper.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/common/common.min.js"></script>
    <script src="/vendor/jquery.validation/jquery.validate.min.js"></script>
    <script src="/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="/vendor/jquery.gmap/jquery.gmap.min.js"></script>
    <script src="/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
    <script src="/vendor/isotope/jquery.isotope.min.js"></script>
    <script src="/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="/vendor/vide/jquery.vide.min.js"></script>
    <script src="/vendor/vivus/vivus.min.js"></script>
    <!-- Theme Base, Components and Settings -->
    <script src="/js/theme.js"></script>
    <!-- Current Page Vendor and Views -->
    <script src="/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Theme Custom -->
    <script src="/js/custom.js"></script>
    <!-- Theme Initialization Files -->
    <script src="/js/theme.init.js"></script>
    <!-- Config JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="/js/config.js"></script>
    <!-- CUSTOM JS -->
    @yield('js')

</body>
</html>