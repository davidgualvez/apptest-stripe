<footer id="footer">
    <div class="container">
        <div class="footer-ribbon">
            <span>Get in Touch</span>
        </div>
        <div class="row py-5 my-4">
            <div class="col-md-6 mb-4 mb-lg-0">
                {{--<a href="/" class="logo pr-0 pr-lg-3">
                <img alt="Porto Website Template" src="/img/logo-footer-trans.png" class="opacity-7 bottom-4" height="33">
                </a>
                <p class="mt-2 mb-2">We felt like, in Singapore, PC enthusiast such as ourselves, don’t get any recognition or any sort of reward for the hard work and time we spent on our own PC build.<br /><br />
                At PC BattleZone, We created a platform for PC enthusiast in Singapore to battle out with their custom builds and get a little reward from us to show our appreciation.</p>
                --}}
            </div>
            <div class="col-md-6">

                <h5 class="text-3 mb-3">Link Map</h5>
                <div class="row">
                <div class="col-md-6">
                    {{--
                    <ul class="list list-icons list-icons-lg">
                        <li class="mb-1">
                            <i class="far fa-dot-circle text-color-primary"></i>
                            <p class="m-0">Singapore</p>
                        </li>
                        <li class="mb-1">
                            <i class="fab fa-whatsapp text-color-primary"></i>
                            <p class="m-0"><a href="tel:8001234567">(65) 9450 1363</a></p>
                        </li>
                        <li class="mb-1">
                            <i class="far fa-envelope text-color-primary"></i>
                            <p class="m-0"><a href="mailto:mail@example.com">support@pcbattlezone.com</a></p>
                        </li>
                    </ul>
                    --}}
                </div>
                <div class="col-md-6">
                    <ul class="list list-icons list-icons-sm">
                        <li><i class="fas fa-angle-right"></i><a href="{{ route('shop') }}" class="link-hover-style-1 ml-1"> About Us</a></li>
                        <li><i class="fas fa-angle-right"></i><a href="/submit-entry" class="link-hover-style-1 ml-1"> Submit Entry</a></li>

                        {{--<li><i class="fas fa-angle-right"></i><a href="/contact" class="link-hover-style-1 ml-1"> Contact Us</a></li>
                        <li><i class="fas fa-angle-right"></i><a href="/terms-of-service" class="link-hover-style-1 ml-1"> Terms of Service</a></li>
                        <li><i class="fas fa-angle-right"></i><a href="/privacy-policy" class="link-hover-style-1 ml-1">Privacy Policy</a></li>
                        --}}
                    </ul>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright footer-copyright-style-2">
        <div class="container py-2">
            <div class="row py-4">
                <div class="col d-flex align-items-center justify-content-center">
                <p>© Copyright 2019. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
