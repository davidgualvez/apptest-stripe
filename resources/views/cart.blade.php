@extends('layouts.app')

@section('title', 'Cart')

@section('js')
<script>
</script>
@endsection

@section('content')

    <section class="page-header page-header-classic page-header-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                    <h1 data-title-border>Cart</h1>
                </div>
                <div class="col-md-4 order-1 order-md-2 align-self-center">
                    <ul class="breadcrumb d-block text-md-right">
                        <li><a href="javascript:;">Cart</a></li>
                        <li class="active">Items</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container">


        <div class="row">
            <div class="col">

                <div class="featured-boxes">
                    <div class="row">
                        <div class="col">
                            <div class="featured-box featured-box-primary text-left mt-2" style="">
                                <div class="box-content">
                                    <form method="post" action="">
                                        <table class="shop_table cart">
                                            <thead>
                                                <tr>
                                                    <th class="product-remove">
                                                        &nbsp;
                                                    </th>
                                                    <th class="product-thumbnail">
                                                        &nbsp;
                                                    </th>
                                                    <th class="product-name">
                                                        Product
                                                    </th>
                                                    <th class="product-price">
                                                        Price
                                                    </th>
                                                    <th class="product-quantity">
                                                        Quantity
                                                    </th>
                                                    <th class="product-subtotal">
                                                        Total
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach( $carts as $key=>$cart)
                                                <tr class="cart_table_item">
                                                    <td class="product-remove">
                                                        <a href="{{ route('cart.remove', ['key' => encrypt($cart->id) ]) }}" title="Remove this item" class="remove" href="#">
                                                            <i class="fas fa-times"></i>
                                                        </a>
                                                    </td>
                                                    <td class="product-thumbnail">
                                                        <a href="shop-product-sidebar-left.html">
                                                            <img width="100" height="100" alt="" class="img-fluid" src="{{ $cart->product->photo }}">
                                                        </a>
                                                    </td>
                                                    <td class="product-name">
                                                        <a href="shop-product-sidebar-left.html">{{ $cart->product->name }}</a>
                                                    </td>
                                                    <td class="product-price">
                                                        <span class="amount">${{ $cart->product->price }}</span>
                                                    </td>
                                                    <td class="product-quantity">
                                                        <b>{{  $cart->qty }}</b>
                                                            <!-- <div class="quantity">
                                                                <input type="button" class="minus" value="-">
                                                                <input type="text" class="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1">
                                                                <input type="button" class="plus" value="+">
                                                            </div> -->
                                                        
                                                    </td>
                                                    <td class="product-subtotal">
                                                        <span class="amount">${{ $cart->sub_total }}</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                 
                                            </tbody>
                                        </table>
                                    
                                </form></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="featured-boxes">
                    <div class="row">
                        <!-- <div class="col-sm-6">
                            <div class="featured-box featured-box-primary text-left mt-3 mt-lg-4" style="height: 434.8px;">
                                <div class="box-content">
                                    <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Calculate Shipping</h4>

                                    <form action="/" id="frmCalculateShipping" method="post">
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label class="font-weight-bold text-dark">Country</label>
                                                <select class="form-control">
                                                    <option value="">Select a country</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <label class="font-weight-bold text-dark">State</label>
                                                <input type="text" value="" class="form-control">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label class="font-weight-bold text-dark">Zip Code</label>
                                                <input type="text" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <input type="submit" value="Update Totals" class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase" data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-sm-6">
                            <div class="featured-box featured-box-primary text-left mt-3 mt-lg-4" style="height: 434.8px;">
                                <div class="box-content">
                                    <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Cart Totals</h4>
                                    <table class="cart-totals">
                                        <tbody>
                                            <tr class="cart-subtotal">
                                                <th>
                                                    <strong class="text-dark">Cart Subtotal</strong>
                                                </th>
                                                <td>
                                                    <strong class="text-dark"><span class="amount">${{ $carts->sum('sub_total') }}</span></strong>
                                                </td>
                                            </tr>
                                            <tr class="shipping">
                                                <th>
                                                    Shipping
                                                </th>
                                                <td>
                                                    Free Shipping<input type="hidden" value="free_shipping" id="shipping_method" name="shipping_method">
                                                </td>
                                            </tr>
                                            <tr class="total">
                                                <th>
                                                    <strong class="text-dark">Order Total</strong>
                                                </th>
                                                <td>
                                                    <strong class="text-dark"><span class="amount">${{ $carts->sum('sub_total') }}</span></strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                @if( $carts->count() > 0)    
                                                    <form action="/checkout" method="POST">
                                                        @csrf
                                                        <script
                                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                            data-key="{{ config('services.stripe.key') }}"
                                                            data-amount="{{ $carts->sum('sub_total') }}00"
                                                            data-name="Keep Things Up"
                                                            data-description="Widget"
                                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                                            data-locale="auto">
                                                        </script>
                                                    </form>
                                                @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- <div class="row">
                    <div class="col">
                        <div class="actions-continue">
                            <button type="submit" class="btn btn-primary btn-modern text-uppercase">Proceed to Checkout <i class="fas fa-angle-right ml-1"></i></button>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>
 
        
    </div>

@endsection