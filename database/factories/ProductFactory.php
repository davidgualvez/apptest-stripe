<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'price' => random_int(1,1000),
        'photo' => 'https://source.unsplash.com/300x300/?products'
    ]; 
});
