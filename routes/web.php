<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@shop')->name('shop');

Route::post('/checkout',   'PurchasesController@checkout');

Route::get('/cart', 'CartController@index')->name('cart');
Route::post('/cart',    'CartController@store')->name('add-to-cart');
route::get('/cart/count', 'CartController@count');
route::get('/cart/{key}/remove',    'CartController@destroy')->name('cart.remove');

Route::get('/config', 'ConfigController@updateForm')->name('config');
Route::patch('/config', 'ConfigController@update')->name('config');

