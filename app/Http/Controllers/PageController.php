<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

class PageController extends Controller
{
    //
    public function shop(){
        $products = Product::all();
        return view('shop', compact('products'));
    }
}
