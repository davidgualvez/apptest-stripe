<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cart;
use Stripe\{Stripe, Charge, Customer};

class PurchasesController extends Controller
{
    //
    public function checkout(Request $request){
        
        Stripe::setApiKey( config('services.stripe.secret') );

        $customer = Customer::create([
            'email' => $request->stripeEmail,
            'source' => $request->stripeToken
        ]);

        $carts = Cart::all();
        Charge::create([
            'customer'  => $customer->id,
            'amount'    => $carts->sum('sub_total').'00',
            'currency'  => 'usd'
        ]);

        
        foreach($carts as $c){
            $c->delete();
        }

        return redirect()->route('shop')->with('success', 'Transaction Complete. you may now chill and wait your item to be deliver at your front door!');
    }
}
