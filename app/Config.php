<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    //
    protected $fillable = [
        'stripe_secret_key',
        'stripe_publishable_key'
    ];
}
