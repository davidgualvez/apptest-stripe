"use strict";  

$(document).ready(function(){  
    console.log('profile.js has been loaded.');  

    btnSubmitEmail();
});

function btnSubmitEmail(){
    $('#btn-update-pn').on('click', function(){
        
        var mobile_number = $('#mobile-number');

        // validation
        if( mobile_number.val() == ''){
            console.log('Invalid mobile number');
            mobile_number.focus();
            return;
        }

        var data = {
            _method : 'PATCH',
            mobile_number : mobile_number.val(),
            tag : 'mobile_number'
        };

        post('/user/self', data, function(response){
            if(response.success == false){
                console.log(response.message);

                mobile_number.parent().next().html(response.message);
                mobile_number.addClass('is-invalid');

                return;
            }

            // success
            console.log(response.message);
            redirectTo('/home');
        });

    });
}