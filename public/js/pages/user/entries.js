"use strict";  

$(document).ready(function(){  
    console.log('entries.js has been loaded.');  
    
    btnView();
});

function btnView(){
    $('.btn-view').on('click', function(){
        var id = $(this).data('id'); 
        redirectTo('/user/entry/'+id);
    });
}