"use strict"; 
// Disable auto discover for all elements:
Dropzone.autoDiscover = false; 

$(document).ready(function(){  
    console.log('entry.js has been loaded.');

 
    // ===============
    var myDropzone = new Dropzone('div#imageUpload', {
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        //maxFiles: 0,
        paramName: 'photos',
        clickable: true,
        url: '/user/entry/'+ $('#entry-id').val(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function () {
            
            var myDropzone = this;
            var form = $('#imageUploadForm'); 
            
            this.on('sending', function (file, xhr, formData) { 
                // Append all form inputs to the formData Dropzone will POST
                var data = form.serializeArray(); 
                $.each(data, function (key, el) { 
                    formData.append(el.name, el.value);
                });
                formData.append('_method','PATCH'); 
            });

            this.on("error", function(file, message) { 
                alert('asd: '+message);
                this.removeFile(file);  
            }); 

            $('#btnUpdate').click(function (e) { 
                e.preventDefault();
                e.stopPropagation();

                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {      
                    // No file selected for upload 
                    /**
                     * 
                     */
                    post(
                        '/user/entry/'+ $('#entry-id').val(),
                        {
                            _method         : 'PATCH',
                            id              : $('#entry-id').val(),
                            title           : $('#title').val(),
                            description     : $('#description').val()
                        },
                    function(response){ 
                        if(response.success == false){
                            showAlert('Warming',response.message,'alert-warning','alert-container');
                            return;
                        }
                        showAlert('',response.message,'alert-success','alert-container');
                    });
                    return false;
                }

                if ( form.valid() ) { 
                    myDropzone.processQueue();   
                    return;
                }  

            }); 
 
        },
        error: function (file, response){
            
            if ($.type(response) === "string")
                var message = response; //dropzone sends it's own error messages in string
            else
                var message = response.message;
            file.previewElement.classList.add("dz-error");
            var _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
            var _results = [];
            for (var _i = 0, _len = _ref.length; _i < _len; _i++) {
                var node = _ref[_i];
                _results.push(node.textContent = message);
            }
            return _results;
        },
        successmultiple: function (file, response) { 
            console.log(response); 
            if(response.success == false){
                showAlert('Warming',response.message,'alert-warning','alert-container');
                return;
            }
            showAlert('',response.message,'alert-success','alert-container');
            setTimeout(() => {
                redirectTo('');
            }, 3000);
            this.removeAllFiles(true);
        },
        completemultiple: function (file, response) { 
           // console.log('completemultiple'.file, response, "completemultiple");
        },
        reset: function () { 
            console.log("resetFiles:");
            this.removeAllFiles(true);
        },
    });
    // ===============

    btnRemove();

    /*
	Dialog with CSS animation
	*/
	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	});

});

function btnRemove(){
    $('#btn-remove').on('click', function(){
        var self = $(this);

        console.log('btn-remove :' + self.data('id'));
        var data = {
            _method : 'DELETE'
        };

        post('/user/entry/'+ self.data('id') +'/remove', data, function(response){
            console.log(response);
            if(response.success == false){
                redirectTo('');
                return;
            }

            setTimeout(() => {
                redirectTo('/user');
            }, 1000);
        });
    });
}