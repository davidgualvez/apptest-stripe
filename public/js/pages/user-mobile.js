"use strict";  

$(document).ready(function(){  
    console.log('user-mobile.js has been loaded.');  

    btnUpdate();
});

function btnUpdate(){
    $('#btn-update').on('click', function(){ 

        var mobile_number = $('#mobile-number');

        // validation
        if( mobile_number.val().trim() == ''){ 
            mobile_number.parent().next().html('Mobile number is required');
            mobile_number.addClass('is-invalid');
            mobile_number.removeClass('is-valid');

            mobile_number.val('');
            mobile_number.focus();
            return;
        }
        console.log( validateContactNumber(mobile_number.val().trim()) );
        if( !validateContactNumber(mobile_number.val().trim()) ){
            mobile_number.parent().next().html('Must be a valid Singapore Mobile Number.');
            mobile_number.addClass('is-invalid');
            mobile_number.removeClass('is-valid');

            mobile_number.val('');
            mobile_number.focus();
            return;
        }

        var data = {
            _method : 'PATCH',
            mobile_number : mobile_number.val(),
            tag : 'mobile_number'
        };
        

        post('/user/self', data, function(response){
            if(response.success == false){
                console.log(response.message); 
                mobile_number.parent().next().html(response.message);
                mobile_number.addClass('is-invalid');
                mobile_number.removeClass('is-valid');
                return;
            }

            // success
            mobile_number.parent().next().html('');
            mobile_number.removeClass('is-invalid');
            mobile_number.addClass('is-valid');  

            setTimeout(() => {
                redirectTo('');
            }, 2000);

        });

    });
}