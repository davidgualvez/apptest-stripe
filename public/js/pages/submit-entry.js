"use strict"; 
// Disable auto discover for all elements:
Dropzone.autoDiscover = false;  

$(document).ready(function(){  
    console.log('submit-entry.js has been loaded.'); 

    var myDropzone = new Dropzone('div#imageUpload', {
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 5,
        paramName: 'photos',
        clickable: true,
        url: '/submit-entry',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        acceptedFiles: 'image/*',
        init: function () {
    
            var myDropzone = this;
            var form = $('#imageUploadForm');
            // Update selector to match your button
            $('#btnSubmit').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {      
                    // No file selected for upload 
                    /**
                     * 
                     */ 
                    showAlert('Warning','Must have atleast 1 Photo.','alert-warning','alert-container');
                    return false;
                }

                if ( form.valid() ) {
                    myDropzone.processQueue();
                }
                //return false;
            });
    
            this.on('sending', function (file, xhr, formData) {
                // Append all form inputs to the formData Dropzone will POST
                var data = form.serializeArray();
                //xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                
                // $.each(data, function (key, el) { 
                //     formData.append(el.name, el.value);
                // });
                var title       = $('#title');
                var desc        = $('#description'); 
                var otp_code    = $('#otp_code'); 
                var setup_cost  = $('#setup_cost');

                formData.append('title', title.val());
                formData.append('description', desc.val());
                formData.append('otp_code', otp_code.val());
                formData.append('setup_cost', setup_cost.val());

                console.log(formData); 
            });

            this.on("error", function(file, message) { 
                alert(message);
                this.removeFile(file); 
            });

        },
        error: function (file, response){
            if ($.type(response) === "string")
                var message = response; //dropzone sends it's own error messages in string
            else
                var message = response.message;
            file.previewElement.classList.add("dz-error");
            var _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
            var _results = [];
            for (var _i = 0, _len = _ref.length; _i < _len; _i++) {
                var node = _ref[_i];
                _results.push(node.textContent = message);
            }
            return _results;
        },
        successmultiple: function (file, response) {

            var title       = $('#title');
            var desc        = $('#description');
            var otp_code    = $('#otp_code');
            var setup_cost  = $('#setup_cost');

            if(response.success == false){
                console.log(response);
                showAlert('Warning',response.message,'alert-warning','alert-container'); 
                
                if(response.element_id == 'description'){ 
                    desc.removeClass('is-valid');
                    desc.addClass('is-invalid'); 
                }

                if(response.element_id == 'otp_code'){ 
                    otp_code.removeClass('is-valid');
                    otp_code.addClass('is-invalid'); 
                }
                
                this.removeAllFiles(true);
                // setTimeout(() => {
                //     $('.alert-warning').remove();
                // }, 2000);
                return;
            }

            showAlert('Success',response.message,'alert-success','alert-container');

            /**
             * RESET
             */
            title.val('');
            title.removeClass('is-valid');
            title.removeClass('is-invalid');
            desc.val('');
            tinymce.get('description').setContent('');
            desc.removeClass('is-valid');
            desc.removeClass('is-invalid');
            setup_cost.val('');
            setup_cost.removeClass('is-valid');
            setup_cost.removeClass('is-invalid');
            otp_code.val('');
            otp_code.removeClass('is-valid');
            otp_code.removeClass('is-invalid'); 
            this.removeAllFiles(true);

            setTimeout(() => {
                $('.alert-success').remove();
            }, 5000);

        },
        completemultiple: function (file, response) {
            //console.log(file, response, "completemultiple");
            //$modal.modal("show");
        },
        reset: function () {
            //console.log("resetFiles");
            this.removeAllFiles(true);
        }
    });

    btnRequestCode();
});

function btnRequestCode(){
    $('#btn-request-code').on('click', function(){ 
        get('/otp/code', {}, function(response){
            console.log(response);
            if(response.success == false){
                showAlert('Warning',response.message,'alert-warning','alert-container');
                return;
            }
            showAlert('Success',response.message,'alert-info','alert-container');
        });
    });
}


 
